﻿var counter;
var hand;
var handRechts;
var imgArray = new Array();
var index;
var ventje;

// Lees de afbeeldingen voor het draadventje in in een array
for(i=0;i<=18;i++){
	imgArray[i] = new Image();
	imgArray[i].src = 'images/Draadventje_' + '0' + (i+484)  + '.png';
}


// Functie die wordt opgestart vanuit HTML
var startAnimation = function() {
    // achtergrond met lijn
    achtergrond2 = document.getElementById('achtergrond2');
    // tweede ventje
    draadventje = document.getElementById('draadventje');
	// hand die tekent
    hand = document.getElementById('hand');
    // eerste ventje
    ventje = document.getElementById('ventje');

    // Waar start achtergrond met lijn
    achtergrondRechts = 1674;
	counter = 0;
	// Waar start tekenende hand
    handRechts = 1664;
    // Waar start wandelend ventje
    ventjeRechts = 1674;

	// Laat de hand lijn tekenen
    timer = window.setInterval(moveHand, 10);
}

var moveHand = function() {
	// Hand telkens met 2 pixels opschuiven
	handRechts -= 2;
	// Achtergrond telkens met 2 pixels opschuiven
	achtergrondRechts -= 2;
	hand.style.left = handRechts + 'px';
	achtergrond2.style.left = achtergrondRechts + 'px';
	counter++;
	// Hand laten stoppen en laten verdwijnen
	if(counter > 625) {
		// Laat de timer stoppen
		clearInterval(timer);
		counter = 0;
		// Even wachten om hand te laten verdwijnen
		window.setTimeout(verwijderHand, 1000);
	}
}

var verwijderHand = function() {
		// Laat de hand verdwijnen
		hand.style.visibility = 'hidden';
		// Laat het ventje wandelen
		timer = window.setInterval(moveVentje, 20);
}

var moveVentje = function() {
	// laat de animated gif telkens met 2 pixels opschuiven
	ventjeRechts -= 2;
	ventje.style.left = ventjeRechts + 'px';
	counter++;
	if(counter > 630) {
		// Laat het ventje stoppen
		clearInterval(timer);
		counter = 0;
		ventje.style.visibility = 'hidden';
		// Vervang animated gif door afbeelding van draadventje
		draadventje.style.visibility = 'visible';
		index = 0;
		// Laat de afbeeldingen van het draadventje elkaar afwisselen
		timer = window.setInterval(beweegVentje, 160);
	}
}


var beweegVentje = function() {
	draadventje.src=imgArray[index].src;
	index++;
	if(index>=imgArray.length) {
		clearInterval(timer);
		counter = 0;
		window.setTimeout(verwijderVentje, 1000);
		
	}
}

var verwijderVentje = function() {
	hand.style.visibility = 'visible';
	handRechts = 300;
	timer = window.setInterval(moveHand2, 10);
}

var moveHand2 = function() {
	handRechts -= 2;
	achtergrondRechts -= 2;
	hand.style.left = handRechts + 'px';
	achtergrond2.style.left = achtergrondRechts + 'px';
	counter++;
	if(counter > 220) {
		clearInterval(timer);
		counter = 0;
		hand.style.visibility = 'hidden';
		draadventje.style.visibility = 'hidden';
		ventje.style.visibility = 'visible';
		timer = window.setInterval(moveVentje2, 10);
	}
}

var moveVentje2 = function() {
	ventjeRechts -= 2;
	ventje.style.left = ventjeRechts + 'px';
	counter++;
	if(counter > 630) {
		clearInterval(timer);
	}
}




